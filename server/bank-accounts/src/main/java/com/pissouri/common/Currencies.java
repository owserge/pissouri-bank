package com.pissouri.common;

import java.util.*;

import static java.util.Arrays.asList;

/**
 * Common static utility methods for {@link Currency}
 */
@SuppressWarnings("WeakerAccess")
public final class Currencies {

    public static final Currency EUR = Currency.getInstance("EUR"); // Euro
    public static final Currency GBP = Currency.getInstance("GBP"); // British Pound
    public static final Currency AUD = Currency.getInstance("AUD"); // Australian Dollar
    public static final Currency USD = Currency.getInstance("USD"); // United States Dollar

    private static final Set<Currency> AVAILABLE = new HashSet<>(asList(EUR, GBP, AUD, USD));

    /**
     * @return Set of application-supported {@link Currency} definitions
     */
    public static Set<Currency> available() {

        return Collections.unmodifiableSet(AVAILABLE);
    }

    /**
     * @return Whether or not the {@link Currency} provided is supported by our application
     */
    public static boolean available(Currency currency) {

        return Objects.nonNull(currency) && AVAILABLE.contains(currency);
    }

    /**
     * @return Whether or not this currency code represents an application-supported {@link Currency}
     */
    public static boolean available(String currencyCode) {

        if (Objects.isNull(currencyCode)) return false;
        try {
            return available(Currency.getInstance(currencyCode));

        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
