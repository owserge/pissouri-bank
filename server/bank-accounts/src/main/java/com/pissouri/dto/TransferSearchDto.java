package com.pissouri.dto;

import com.pissouri.common.TransferStatusCodes;
import com.pissouri.common.TransferTypeCodes;
import com.pissouri.validation.constraint.AvailableCurrency;
import com.pissouri.validation.constraint.ValidTransferStatus;
import com.pissouri.validation.constraint.ValidTransferType;

import java.util.Objects;

/**
 * A {@link PageableDto} set of search parameters, used for filtering through a list of {@link TransferDto}
 */
public class TransferSearchDto implements PageableDto {

    /**
     * Type of bank transfer, as defined by {@link TransferTypeCodes}
     */
    @ValidTransferType
    private String type;

    /**
     * Status of bank transfer, as defined by {@link TransferStatusCodes}
     */
    @ValidTransferStatus
    private String status;

    /**
     * The bank transfer currency code
     */
    @AvailableCurrency
    private String currency;

    /**
     * Number of transfers per page
     */
    private Integer size;

    /**
     * The transfers page to be retrieved
     */
    private Integer page;

    public String getType() {

        return type;
    }

    public TransferSearchDto setType(String type) {

        this.type = type;
        return this;
    }

    public String getStatus() {

        return status;
    }

    public TransferSearchDto setStatus(String status) {

        this.status = status;
        return this;
    }

    public String getCurrency() {

        return currency;
    }

    public TransferSearchDto setCurrency(String currency) {

        this.currency = currency;
        return this;
    }

    @Override
    public Integer getSize() {

        return size;
    }

    public TransferSearchDto setSize(Integer size) {

        this.size = size;
        return this;
    }

    @Override
    public Integer getPage() {

        return page;
    }

    public TransferSearchDto setPage(Integer page) {

        this.page = page;
        return this;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransferSearchDto that = (TransferSearchDto) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(status, that.status) &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(size, that.size) &&
                Objects.equals(page, that.page);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, status, size, page);
    }

    @Override
    public String toString() {

        return "TransferSearchDto{" +
                "type='" + type + '\'' +
                ", status='" + status + '\'' +
                ", currency='" + currency + '\'' +
                ", size=" + size +
                ", page=" + page +
                '}';
    }
}
