package com.pissouri.data;

import com.pissouri.common.TransferStatusCodes;
import com.pissouri.common.TransferTypeCodes;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.pissouri.common.TransferTypeCodes.INCOMING;
import static com.pissouri.common.TransferTypeCodes.OUTGOING;

/**
 * JPA specification for {@link Transfer} records
 */
public class TransferSpecification implements Specification<Transfer> {

    /** The identifier of the {@link Account} the record should be associated with */
    private Long accountId;

    /** The status of a record, as per {@link TransferStatusCodes} */
    private String status;

    /** The type of a record, as per {@link TransferTypeCodes} */
    private String type;

    /** The bank transfer currency code */
    private String currency;

    public TransferSpecification setAccountId(Long accountId) {

        this.accountId = accountId;
        return this;
    }

    public TransferSpecification setStatus(String status) {

        this.status = status;
        return this;
    }

    public TransferSpecification setType(String type) {

        this.type = type;
        return this;
    }

    public TransferSpecification setCurrency(String currency) {

        this.currency = currency;
        return this;
    }

    /**
     * @return A {@link Predicate} composed of conditions based on the class fields
     */
    @Override
    public Predicate toPredicate(Root<Transfer> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        List<Predicate> predicates = new ArrayList<>();

        if (Objects.nonNull(accountId)) {
            Path<Account> account = root.get("account");
            predicates.add(builder.equal(account.get("id"), accountId));
        }

        if (Objects.nonNull(status)) {
            predicates.add(builder.equal(root.get("status"), status));
        }

        if (Objects.nonNull(type)) {
            if (INCOMING.equals(type)) predicates.add(builder.greaterThan(root.get("amount"), 0));
            if (OUTGOING.equals(type)) predicates.add(builder.lessThan(root.get("amount"), 0));
        }

        if (Objects.nonNull(currency)) {
            predicates.add(builder.equal(root.get("currency"), currency));
        }

        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
