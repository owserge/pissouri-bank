package com.pissouri.validation.constraint;

import com.pissouri.common.TransferStatusCodes;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.util.Objects;

@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidTransferStatus.Validator.class })
public @interface ValidTransferStatus {

    String message() default "Invalid status parameter";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Asserts if a value is valid according to {@link TransferStatusCodes}
     */
    class Validator implements ConstraintValidator<ValidTransferStatus, String> {

        /**
         * @return True if value is NULL or valid, false if otherwise
         */
        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {

            return Objects.isNull(value) || TransferStatusCodes.valid(value);
        }
    }
}
