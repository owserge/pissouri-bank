package com.pissouri.validation.constraint;

import com.pissouri.common.Currencies;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.util.Objects;

@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { AvailableCurrency.Validator.class })
public @interface AvailableCurrency {

    String message() default "Invalid currency parameter, or currency not supported";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Asserts if value represents an application-supported currency code, as per {@link Currencies}
     */
    class Validator implements ConstraintValidator<AvailableCurrency, String> {

        /**
         * @return True if value is NULL or valid and supported, false if otherwise
         */
        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {

            return (Objects.isNull(value)) || Currencies.available(value);
        }
    }
}
