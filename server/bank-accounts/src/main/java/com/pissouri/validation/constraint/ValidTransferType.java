package com.pissouri.validation.constraint;

import com.pissouri.common.TransferTypeCodes;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.util.Objects;

@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidTransferType.Validator.class })
public @interface ValidTransferType {

    String message() default "Invalid type parameter";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Asserts if a value is valid according to {@link TransferTypeCodes}
     */
    class Validator implements ConstraintValidator<ValidTransferType, String> {

        /**
         * @return True if the parameter is NULL or valid, false if otherwise
         */
        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {

            return Objects.isNull(value) || TransferTypeCodes.valid(value);
        }
    }
}
