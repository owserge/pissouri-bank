package com.pissouri.validation;

import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;

import java.util.Objects;

/**
 * Static {@link FieldError} utility methods
 */
public final class FieldErrors {

    /**
     * @return The first {@link FieldError} of this {@link BindException}, if one exists, NULL if otherwise
     */
    public static FieldError ofException(BindException exception) {

        if (Objects.isNull(exception) || exception.getFieldErrors().isEmpty()) return null;
        return exception.getFieldErrors().get(0);
    }

    /**
     * @return The error message of a {@link FieldError} instance, if one exists, some other message if otherwise
     */
    public static String getMessageOrElse(FieldError fieldError, String defaultMessage) {

        if (Objects.isNull(fieldError) || Objects.isNull(fieldError.getDefaultMessage())) return defaultMessage;
        return fieldError.getDefaultMessage();
    }
}
