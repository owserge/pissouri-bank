package com.pissouri.validation;

/**
 * Static utility methods for basic "primitive" argument validation
 */
public final class Arguments {

    /**
     * Throws an {@link IllegalArgumentException} if the condition is not satisfied
     */
    public static void must(Object value, boolean condition) {

        if (!condition) throw new IllegalArgumentException(String.format("Invalid argument %s", value));
    }
}
