package com.pissouri.common;

import org.junit.Test;

import java.util.Set;

import static com.pissouri.common.TransferStatusCodes.*;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferStatusCodesUTest {

    @Test
    public void all_returnsSetOfApplicationSupportedTransferStatusCodes() {

        Set<String> statusCodeSet = TransferStatusCodes.all();

        assertThat(statusCodeSet).hasSize(3);

        assertThat(statusCodeSet).contains(PENDING);
        assertThat(statusCodeSet).contains(ACCEPTED);
        assertThat(statusCodeSet).contains(REJECTED);

        assertThat(statusCodeSet).contains("PENDING");
        assertThat(statusCodeSet).contains("ACCEPTED");
        assertThat(statusCodeSet).contains("REJECTED");
    }

    @Test
    public void valid_returnsTrue_whenValueIsValidTransferStatusCode() {

        assertThat(valid(PENDING)).isTrue();
        assertThat(valid(ACCEPTED)).isTrue();
        assertThat(valid(REJECTED)).isTrue();

        assertThat(valid("PENDING")).isTrue();
        assertThat(valid("ACCEPTED")).isTrue();
        assertThat(valid("REJECTED")).isTrue();
    }

    @Test
    public void valid_returnsFalse_WhenValueIsNotValidTransferStatusCode() {

        assertThat(valid("FOO")).isFalse();
    }
}
