package com.pissouri.common;

import org.junit.Test;

import java.util.Set;

import static com.pissouri.common.TransferTypeCodes.*;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferTypeCodesUTest {

    @Test
    public void all_returnsSetOfApplicationSupportedTransferTypeCodes() {

        Set<String> typeCodeSet = TransferTypeCodes.all();

        assertThat(typeCodeSet).hasSize(2);

        assertThat(typeCodeSet).contains(INCOMING);
        assertThat(typeCodeSet).contains(OUTGOING);

        assertThat(typeCodeSet).contains("IN");
        assertThat(typeCodeSet).contains("OUT");
    }

    @Test
    public void valid_returnsTrue_whenValueIsValidTransferTypeCode() {

        assertThat(valid(INCOMING)).isTrue();
        assertThat(valid(OUTGOING)).isTrue();

        assertThat(valid("IN")).isTrue();
        assertThat(valid("OUT")).isTrue();
    }

    @Test
    public void valid_returnsFalse_WhenValueIsNotValidTransferTypeCode() {

        assertThat(valid("FOO")).isFalse();
    }
}
